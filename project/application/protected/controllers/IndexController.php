<?php

class IndexController extends Controller
{
    public $layout='//layouts/main';

    public function actionIndex()
    {
        $jq_loader = $this->getJQLoader();
        $rendered_grid = $jq_loader->render('jqTask');
        $this->render('index', array('rendered_grid' => $rendered_grid));
    }
}
