<?php

class jqTask extends jqGrid
{
    protected function init()
    {
        /*$this->query = "
          SELECT tbs.cx, tbs.title, tbr.NDC 
          FROM `tb_source` tbs 
          LEFT JOIN `tb_rel` tbr on tbs.cx = tbr.cx 
          WHERE tbs.`title` like 'title 1%' 
        ";*/
        $this->query = "
          SELECT tbs.cx, tbs.title, (SELECT ndc from tb_rel where cx = tbs.cx limit 1) as NDC
          FROM `tb_source` tbs 
          where tbs.`title` like 'title 1%' 
          ";

        /*$this->count_query = "
          SELECT count(tbs.cx) as cnt
          FROM `tb_source` tbs 
          LEFT JOIN `tb_rel` tbr on tbs.cx = tbr.cx 
          WHERE tbs.`title` like 'title 1%'";
        */

        $this->count_query = "
          SELECT count(tbs.cx) as cnt
          FROM `tb_source` tbs 
          where tbs.`title` like 'title 1%'
        ";
        $this->primary_key = 'cx';
        $this->sidx = 'cx';

        #Make all columns editable by default
        $this->cols_default = array('editable' => false);

        #Set columns
        $this->cols = array(
            'cx' => array('label' => 'CX',
                'width' => 250,
                'editable' => false,
            ),
            'title' => array('label' => 'title',
                'width' => 250,
                'editable' => false,
            ),
            'NDC' => array('label' => 'NDC',
                'width' => 250,
                'editable' => false,
            ),
        );


        $this->nav = array('add' => false, 'edit' => false, 'del' => false);

        #Add filter toolbar
        $this->render_filter_toolbar = false;
    }
}