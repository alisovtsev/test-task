<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    function init()
    {
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']))
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function renderScriptPipeline($scripts = array())
    {
        foreach ($scripts as $type => $items) {
            echo "\n";
            foreach ($items as $item) {
                echo "    ";
                if ($type == 'js') {
                    echo '<script type="text/javascript" src="' . Yii::app()->request->baseUrl . $item . '"></script>';
                } else {
                    $item .= '?' . time();
                    echo '<link rel="stylesheet" type="text/css" href="' . Yii::app()->request->baseUrl. $item . '" media="screen">';
                }
                echo "\n";
            }
        }
    }

    public function getJQLoader()
    {

#Init loader
        $jq_loader = new jqGridLoader;

#Set grid_path
        $jq_loader->set('grid_path', AP . '/protected/models/grids/');

#Set PDO options
        $jq_loader->set('pdo_dsn' , 'mysql:host=localhost;dbname=new_test');
        $jq_loader->set('pdo_user', 'root');
        $jq_loader->set('pdo_pass', '');

#Turn on debug output
        $jq_loader->set('debug_output', true);

#Autorun grid OUTPUT or OPER, if possible
#Script may 'exit' here!
        $jq_loader->set('debug_output', true);

        $jq_loader->autorun();
        return $jq_loader;
    }
}