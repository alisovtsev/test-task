<?php
if (YII_DEBUG) {
    ini_set('display_errors', 1);
    error_reporting(2047);
}

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Test task',
    'language' => 'en',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'system.web.*',
        'application.helpers.*',
        'application.jqGrid.*',
        'application.jqGrid.Exception.*',
        'application.jqGrid.DB.*',
        'application.jqGrid.Utils.*',
        'application.jqGrid.Data.*',
        'application.jqGrid.Export.*',
    ),

	'defaultController'=>'index',

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		'db'=>array(
            'class'=>'CDbConnection',
            'connectionString'=>'mysql:host=localhost;dbname=new_test',
            'emulatePrepare' => true,
            'charset' => 'utf8',
            'username'=>'root',
            'password'=>'',
        ),
		'errorHandler'=>array(
			// use 'index/error' action to display errors
			'errorAction'=>'index/error',
		),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            ),
        ),
	),
);