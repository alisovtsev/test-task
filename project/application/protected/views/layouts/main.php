<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <title>Test task jqgrid</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <?php
    $cs = Yii::app()->clientScript;
    $cs->registerCoreScript('jquery');

    $this->renderScriptPipeline(array(
        'css' => array(
            '/css/index.css',
            '/css/redmond/jquery-ui-1.10.3.custom.min.css',
            '/css/ui.jqgrid.css',
        ),
        'js' => array(

            '/js/i18n/grid.locale-en.js',
            '/js/jquery.jqGrid.src.js',
            '/js/jqgrid-ext.js',
        )
    ));
    ?>
    <style>
        .jqgrid-overlay {
            position: absolute;
        }
        .ui-priority-secondary-custom{
            background: url(/css/redmond/images/ui-priority-secondary-custom.png);
        }
    </style>

<?php if (Yii::app()->session->get("to_copy") == 0) : ?>
    <style type="text/css">
    * {
         user-select: none;
        -khtml-user-select: none;
        -o-user-select: none;
        -moz-user-select: -moz-none;
        -webkit-user-select: none;
    }

    ::selection { background: transparent;color:inherit; }
    ::-moz-selection { background: transparent;color:inherit; }
    </style>
<?php endif;?>

    <script>
        var lastSel;
        $.extend($.jgrid.defaults,
            {
                caption: '&nbsp;',
                hidegrid:false,//возможность сворачивание грида
                hoverrows:true,//подсветка при наведении
                scrollOffset:17,//смещение справа для скрола
                width:'100%',
                autowidth: false,
                    // shrinkToFit: false,
                height:'100%',
                autosearch: true,
                            // toolbar: [true,'top'],
                sortname: 'id',
                sortorder: 'desc',
                viewrecords: true,
                rowNum:25,
                rowList:[5,10,20,25,30,40,50],
                altRows:true,
                cellEdit: false,
                altclass:'ui-priority-secondary-custom',
            });
        //$.jgrid.defaults.height = '400px';
        $.jgrid.nav.refreshtext = 'reload';
        $.jgrid.nav.afterRefresh = function() {
            document.getElementById("top_filter").reset();
        };
        $.jgrid.edit.closeAfterEdit = true;
        $.jgrid.edit.closeAfterAdd = true;
        $.jgrid.search.autosearch = true;
        $.jgrid.search.searchOnEnter = false;

    </script>
</head>
<body>
    <?=$content?>
</body>
</html>