function createCookie(a,e,c){if(c){var b=new Date;b.setTime(b.getTime()+864E5*c);c="; expires="+b.toGMTString()}else c="";document.cookie=a+"="+e+c+"; path=/;"}

function readCookie(a){a+="=";for(var e=document.cookie.split(";"),c=0;c<e.length;c++){for(var b=e[c];" "==b.charAt(0);)b=b.substring(1,b.length);if(0==b.indexOf(a))return b.substring(a.length,b.length)}return null}

function eraseCookie(a){createCookie(a,"",-1)}

var dlsec_1,dlsec_2,mdn="კვირა ორშაბათი სამშაბათი ოთხშაბათი ხუთშაბათი პარასკევი შაბათი".split(" "),mes="იანვარი თებერვალი მარტი აპრილი მაისი ივნისი ივლისი აგვისტო სექტემბერი ოქტომბერი ნოემბერი დეკემბერი".split(" ");

function raD(){var a=document.getElementById("chs_1").value,e=document.getElementById("chs_2").value,c=document.getElementById("mes_1").value,c=c-1,b=document.getElementById("mes_2").value,b=b-1,d=document.getElementById("god_1").value,f=document.getElementById("god_2").value,a=new Date(d,c,a,0,0,0),e=new Date(f,b,e,0,0,0);dlsec_1=a.getTime();dlsec_2=e.getTime();b=(dlsec_2-dlsec_1)/24/60/60/1E3;b=(b+"").split(".");0>=b[0]&&(b[0]*=-1);b[0]=dRa(b[0]);document.getElementById("itog").innerHTML=b[0];iDn(a,e)}

function iDn(a,e){var c=a.getDay(),b=e.getDay();document.getElementById("dnd_1").innerHTML=mdn[c]+" &nbsp;";document.getElementById("dnd_2").innerHTML=mdn[b]+" &nbsp;"}

function dRa(a){a+="";for(var e=Math.ceil(a.length/3),c=3-(3*e-a.length),b=c,d=[],f=0;f<e;f++)0==f?d[f]="<b>"+a.substr(0,c)+"</b>":(d[f]="<b>"+a.substr(b,3)+"</b>",b+=3);return d.join(".")}

function shR(){raD();var a=readCookie("shrdn"),a=null==a?dlsec_1+"*****"+dlsec_2:a+"~~~~~"+dlsec_1+"*****"+dlsec_2;16>a.split("~~~~~").length?createCookie("shrdn",a,365):alert("შესაძლებელია მხოლოდ 15 ჩანაწერის დამახსოვრება!");pkShr()}

function udShr(a){if(0<=a&&15>=a){for(var e=readCookie("shrdn").split("~~~~~"),c=e.length,b=[],d=0,f=0;f<c;f++)f!=a&&(b[d]=e[f],d++);0==b.length?(eraseCookie("shrdn"),location.reload()):(a=b.join("~~~~~"),createCookie("shrdn",a,365),pkShr())}else eraseCookie("shrdn"),location.reload()};


   function pkShr()
   {
      var shd = readCookie("shrdn");

      if (shd==null)
      {
         var vstavka = 'არ მოიძებნა';
         var soob_udal = '';
      }
      else
      {
         var dns = shd.split("~~~~~");
         var kls = dns.length;
         var sdn = new Array();

         for (var a=0; a<kls; a++)
         {
            var sa = dns[a].split("*****");
            var itog = ((sa[1]-sa[0])/24/60/60/1000);
            itog = itog + "";
            var itog_1 = itog.split(".");
            if (itog_1[0]<=0) itog_1[0] = itog_1[0]*-1;
            itog_1[0] = dRa(itog_1[0]);

            sa[0] = sa[0]*1;
            var sdfs = new Date(sa[0]);
            var za_1 = new Array();
            za_1[0] = sdfs.getDate();
            za_1[1] = mes[sdfs.getMonth()];
            za_1[2] = sdfs.getFullYear();

            sa[1] = sa[1]*1;
            var sdfs = new Date(sa[1]);
            var za_2 = new Array();
            za_2[0] = sdfs.getDate();
            za_2[1] = mes[sdfs.getMonth()];
            za_2[2] = sdfs.getFullYear();


            var b = a + 1;
            sdn[a] = '<tr>'+
                '<td width="40">&nbsp;</td>'+
                '<td width="30" align="center">'+b+'.</td>'+
                '<td align="left">'+za_1.join(" ")+' – '+za_2.join(" ")+' = <b><span id="itog">'+itog_1[0]+'</span></b> დღე</td>'+
                '<td width="80" align="center"><a href="javascript:udShr('+a+');">'+'წაშლა <img src="../images/trash.png" style="vertical-align: bottom" border="0"></a>'+
                '</td>'+
                '<td width="40" align="left">&nbsp;</td>'+
              '</tr>';
         }
         var vstavka = '<table width="600" border="0" cellpadding="0" cellspacing="0">'+sdn.join("\n")+'</table>';
         var soob_udal = '<table width="600" border="0" cellpadding="0" cellspacing="0">'+
              '<tr>'+
                '<td width="40">&nbsp;</td>'+
                '<td width="30" align="center">&nbsp;</td>'+
                '<td align="right"><a href="javascript:udShr(\'all\');">ყველას წაშლა</a></td>'+
                '<td width="44" style="vertical-align: bottom" align="center"><a href="javascript:udShr(\'all\');">'+
                  '<img src="../images/trash.png" border="0"></a>'+
                '</td>'+
                '<td width="40" align="left">&nbsp;</td>'+
              '</tr>'+
            '</table>';
      }
      document.getElementById("shrez").innerHTML = vstavka;
      document.getElementById("udsln").innerHTML = soob_udal;
   }