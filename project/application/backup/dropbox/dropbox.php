<?PHP
// DROPBOX API UPLOAD

//  CHECK PHP VERSION
if ( version_compare(phpversion(), "5.3") < 0 ) {
  die("<p><b>ERROR</b> PHP 5.3 or greater is required!</p>");
}

require_once(basename(__FILE__)."/../config-dropbox.php");
require_once("lib/DropboxClient.php");
$dropbox = new DropboxClient(array('app_key'=>$boxKey,'app_secret'=>$boxSecret,'app_full_access'=>false,),'en');


// TEST START
#$files[] = 'test.txt'; $files[] = 'test.zip'; echo DropBox_Upload($files);
// TEST ENDE


function DropBox_Upload($files) {
	global $dropbox;
	global $DEMO;
	handle_dropbox_auth($dropbox); // SEE BELOW
	$result = '';
	foreach ($files as $key => $this_file) {
		#echo "<p>Trying to upload ".$this_file."</p>"; // DEBUG ONLY
		if ( file_exists($this_file) OR $DEMO === TRUE) {
			try {
				$result .= "<p>Uploading <b>".basename($this_file)."</b>&hellip;</p>\n";
				if($DEMO==FALSE)$meta = $dropbox->UploadFile($this_file); // upload it!
				#$result .= print_r($meta,true); // DEBUG ONLY
				$result .= "<p>File now in Dropbox&hellip;</p>\n";
			} catch(Exception $e) {
				// TODO mail(htmlspecialchars($e->getMessage()));
				#$result .= "<p>".htmlspecialchars($e->getMessage())."<br><b style='color: red'>ERROR</b></p>\n";
				$result .= "<p><b style='color:red'>Cant't upload to Dropbox&hellip;</b></p>\n"; // ERROR
			}
		}
	} // END foreach
	// WRITE LOG
	$log_dir = dirname(__FILE__).'/log/'."dropbox.txt"; ;
	$fh = fopen($log_dir, 'w') or die("<p><b style='color:red'>Can't write Log-File&hellip;</b></p>"); // ERROR
	fwrite($fh, stripTags($result));
	fclose($fh);
	return $result;
} // END function
function store_token($token, $name) {
	file_put_contents(dirname(__FILE__)."/token/$name.token", serialize($token));
}
function load_token($name) {
	if(!file_exists(dirname(__FILE__)."/token/$name.token")) return null;
	return @unserialize(@file_get_contents(dirname(__FILE__)."/token/$name.token"));
}
function delete_token($name) {
	@unlink(dirname(__FILE__)."/token/$name.token");
}
function handle_dropbox_auth($dropbox) {
	// first try to load existing access token
	$access_token = load_token("access");
	if(!empty($access_token)) {
		$dropbox->SetAccessToken($access_token);
	// are we coming from dropbox's auth page?	
	}elseif(!empty($_GET['auth_callback'])) {
		// then load our previosly created request token
		$request_token = load_token($_GET['oauth_token']);
		if(empty($request_token)) die('Request token not found!');
		// get & store access token, the request token is not needed anymore
		$access_token = $dropbox->GetAccessToken($request_token);
		store_token($access_token, "access");
		delete_token($_GET['oauth_token']);
	}
	// checks if access token is required
	if(!$dropbox->IsAuthorized()) {
		// redirect user to dropbox auth page
		$return_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']."?auth_callback=1";
		$auth_url = $dropbox->BuildAuthorizeUrl($return_url);
		$request_token = $dropbox->GetRequestToken();
		store_token($request_token, $request_token['t']);
		die("<p>Authentication required: <a style=\"color:white;\" href=\"$auth_url\">click</a></p>");
	}
}
function stripTags($string) {
	$string = str_replace('<br>',' ',$string);
	$string = str_replace('&hellip;','�',$string);
	return strip_tags($string);
}
