<?php
// DROPBOX API UPLOAD - FORM 
require_once("dropbox-config.php");
require_once("lib/DropboxClient.php");
$dropbox = new DropboxClient(array('app_key'=>$boxKey,'app_secret'=>$boxSecret,'app_full_access'=>false,),'en');
handle_dropbox_auth($dropbox); // SEE BELOW
if(empty($_FILES['the_upload'])) {
?><tt><form enctype="multipart/form-data" method="POST" action=""><p>
<label for="file">Upload File</label> <input type="file" name="the_upload" />
</p><p><input type="submit" name="submit-btn" value="GO"></p></form>
<?php } else { 
	$upload_name = $_FILES["the_upload"]["name"];
	echo "<tt><p>Uploading '$upload_name'</p>";
	$meta = $dropbox->UploadFile($_FILES["the_upload"]["tmp_name"], $upload_name);
	#print_r($meta); // DEBUG ONLY
	echo "<p><b>Done</b>!</p>";
}
function store_token($token, $name) {
	file_put_contents("token/$name.token", serialize($token));
}
function load_token($name) {
	if(!file_exists("token/$name.token")) return null;
	return @unserialize(@file_get_contents("token/$name.token"));
}
function delete_token($name ){
	@unlink("token/$name.token");
}
function handle_dropbox_auth($dropbox){
	// first try to load existing access token
	$access_token = load_token("access");
	if(!empty($access_token)) {
		$dropbox->SetAccessToken($access_token);
	// are we coming from dropbox's auth page?
	}elseif(!empty($_GET['auth_callback'])) {
		// then load our previosly created request token
		$request_token = load_token($_GET['oauth_token']);
		if(empty($request_token)) die('Request token not found!');
		// get & store access token, the request token is not needed anymore
		$access_token = $dropbox->GetAccessToken($request_token);	
		store_token($access_token, "access");
		delete_token($_GET['oauth_token']);
	}
	// checks if access token is required
	if(!$dropbox->IsAuthorized()) {
		// redirect user to dropbox auth page
		$return_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']."?auth_callback=1";
		$auth_url = $dropbox->BuildAuthorizeUrl($return_url);
		$request_token = $dropbox->GetRequestToken();
		store_token($request_token, $request_token['t']);
		die("<tt>Authentication required: <a href='$auth_url'>Click here</a>.");
	}
}
