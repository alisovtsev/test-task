<?php
/*  SETUP  */

//  PASSWORD - use backup.php?password=...
    $password = 'admin'; // (must change)
    $salt = 'salt'; // (must change)
    $domain = $_SERVER["SERVER_NAME"];

//  USE TEXT ANIMATION
    $VIGOR = TRUE; // SET TO 'FALSE' IF YOU WANT SAVE TIME

//  SCRIPT URL - USED ON CONSOLE - URL TO "backup.php" (must change)
    $scriptUrl = $domain."/backup/backup.php";

//  BACKUP FOLDER - PATH TO FOLDER YOU WANT TO BACKUP (must change)
    // $backupDir = dirname(__FILE__).'/test'; // NO SLASH AT THE END!
    $backupDir = '../../../www'; // NO SLASH AT THE END!

//  BACKUP URL - URL TO FOLDER YOU BACKUP.PHP IS LOCATED (must change)
    $backupUrl = $domain."/backup"; // NO SLASH AT THE END!

//  SET TIMEZONE - USED ON CONSOLE - http://php.net/manual/en/timezones.php
    putenv("TZ=Asia/Tbilisi");                     // PHP 4
    ini_set("date.timezone", "Asia/Tbilisi");      // PHP 4
    date_default_timezone_set("Asia/Tbilisi"); // PHP 5 >= 5.1.0

//  BACKUP NAME - SEE http://php.net/manual/en/function.date.php
    #$filename = date("Y-m-d",time())."-".time()."_backup"; // EVERY start (second) a new Backup
    $filename = date("Y-m-d",time())."_backup";            // only ONE Backup per Day
    #$filename = date("Y-W",time())."_backup";              // only ONE Backup per Week (only PHP 4.1 or >)
    #$filename = date("Y-m",time())."_backup";              // only ONE Backup per Month
    #$filename = date("Y",time())."_backup";                 // only ONE Backup per Year

//  MAIL CONFIG
    $to   = ""; // YOUR EMAIL ADDRESS (ADMIN)
    $toCc = ""; // EMAIL COPY TO... EMAIL-ADDRESS
    $toBcc= ""; // BLIND COPY TO... EMAIL-ADDRESS