<?php

//  ######################################################################################################  //
//
//  TODO
//
//  - FOR INTERN TESTING 
//  - WIN: cmd.exe c:\xampp\php\php.exe backup.php
//  - GMAIL: https://www.google.com/settings/security/lesssecureapps
//
//  ######################################################################################################  //


//  #############  //
//  -------------  //
//  Simple Backup  //
//  v 1.0i 12.12.  //
//  -------------  //
//  #############  //


//  ######################################################################################################  //

//  SYSTEM-CONFIG
$DEMO  = FALSE;     // NEVER SET TO 'TRUE' IN LIVE-SYSTEM
error_reporting(0); // NO ERROR REPORTING
set_time_limit(0);  // NO TIME LIMIT
ini_set('memory_limit','10240M'); // Make sure the script can handle large folders/files

//  ######################################################################################################  //

//  ZIP
$compression = 'TAR';
if (extension_loaded('zip') and class_exists('ZipArchive')) {
	$compression = 'ZIP';
	class FlxZipArchive extends ZipArchive {
		/**
		 * Add a Dir with Files and Subdirs to the archive
		 *
		 * @param string $location Real Location
		 * @param string $name Name in Archive
		 * @author Nicolas Heimann
		 * @access private
		 **/
		public function addDir($location,$name) {
			$this->addEmptyDir($name);
			$this->addDirDo($location, $name);
		 } // EO addDir;
		/**
		 * Add Files & Dirs to archive.
		 *
		 * @param string $location Real Location
		 * @param string $name Name in Archive
		 * @author Nicolas Heimann
		 * @access private
		 **/
		private function addDirDo($location, $name) {
			global $filename;
			global $count;
			$name .= '/';
			$location .= '/';
			// Read all Files in Dir
			$dir = opendir ($location);
			if ( @$_SERVER["SESSIONNAME"] === "Console" or substr( php_sapi_name(), 0, 3 ) == "cli" ) {
			} elseif($count < 1) {
				$count++;
				echo "<p>Read Folder for .ZIP File&hellip;</p>";
			}
			while ($file = readdir($dir))
			{
				if ($file == '.' || $file == '..' || $file == $filename.'.zip') continue;
				// Rekursiv, If dir: FlxZipArchive::addDir(), else ::File();
				$do = (filetype( $location . $file) == 'dir') ? 'addDir' : 'addFile';
				$this->$do($location . $file, $name . $file);
				if ( @$_SERVER["SESSIONNAME"] === "Console" or substr( php_sapi_name(), 0, 3 ) == "cli" ) {
					$z++;
					if ( $z === 10 ) print "[/]\r";
					if ( $z === 20) print "[-]\r";
					if ( $z === 30 ) {
						print "[\]\r";
						$z = 0;
					}
				}
			}
		} // EO addDirDo();
	}
}

//  ######################################################################################################  //

//  JavaScript
if (isset($_GET["javascript"])) {
  header('Content-Type: application/javascript');
  $DIE = '
  // The MIT License (MIT)
  // Typed.js | Copyright (c) 2014 Matt Boldt | www.mattboldt.com
  // Permission is hereby granted, free of charge, to any person obtaining a copy
  // of this software and associated documentation files (the "Software"), to deal
  // in the Software without restriction, including without limitation the rights
  // to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  // copies of the Software, and to permit persons to whom the Software is
  // furnished to do so, subject to the following conditions:
  // The above copyright notice and this permission notice shall be included in
  // all copies or substantial portions of the Software.
  // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  // THE SOFTWARE.
  ! function($) {
    "use strict";
    var Typed = function(el, options) {
        // chosen element to manipulate text
        this.el = $(el);
        // options
        this.options = $.extend({}, $.fn.typed.defaults, options);
        // attribute to type into
        this.isInput = this.el.is("input");
        this.attr = this.options.attr;
        // show cursor
        this.showCursor = this.isInput ? false : this.options.showCursor;
        // text content of element
        this.elContent = this.attr ? this.el.attr(this.attr) : this.el.text()
        // html or plain text
        this.contentType = this.options.contentType;
        // typing speed
        this.typeSpeed = this.options.typeSpeed;
        // add a delay before typing starts
        this.startDelay = this.options.startDelay;
        // backspacing speed
        this.backSpeed = this.options.backSpeed;
        // amount of time to wait before backspacing
        this.backDelay = this.options.backDelay;
        // div containing strings
        this.stringsElement = this.options.stringsElement;
        // input strings of text
        this.strings = this.options.strings;
        // character number position of current string
        this.strPos = 0;
        // current array position
        this.arrayPos = 0;
        // number to stop backspacing on.
        // default 0, can change depending on how many chars
        // you want to remove at the time
        this.stopNum = 0;
        // Looping logic
        this.loop = this.options.loop;
        this.loopCount = this.options.loopCount;
        this.curLoop = 0;
        // for stopping
        this.stop = false;
        // custom cursor
        this.cursorChar = this.options.cursorChar;
        // shuffle the strings
        this.shuffle = this.options.shuffle;
        // the order of strings
        this.sequence = [];
        // All systems go!
        this.build();
    };
    Typed.prototype = {
        constructor: Typed
        ,
        init: function() {
            // begin the loop w/ first current string (global self.strings)
            // current string will be passed as an argument each time after this
            var self = this;
            self.timeout = setTimeout(function() {
                for (var i=0;i<self.strings.length;++i) self.sequence[i]=i;
                // shuffle the array if true
                if(self.shuffle) self.sequence = self.shuffleArray(self.sequence);
                // Start typing
                self.typewrite(self.strings[self.sequence[self.arrayPos]], self.strPos);
            }, self.startDelay);
        }
        ,
        build: function() {
            var self = this;
            // Insert cursor
            if (this.showCursor === true) {
                this.cursor = $("<span class=\"typed-cursor\">" + this.cursorChar + "</span>");
                this.el.after(this.cursor);
            }
            if (this.stringsElement) {
                self.strings = [];
                this.stringsElement.hide();
                var strings = this.stringsElement.find("p");
                $.each(strings, function(key, value){
                    self.strings.push($(value).html());
                });
            }
            this.init();
        }
        // pass current string state to each function, types 1 char per call
        ,
        typewrite: function(curString, curStrPos) {
            // exit when stopped
            if (this.stop === true) {
                return;
            }
            // varying values for setTimeout during typing
            // cant be global since number changes each time loop is executed
            var humanize = Math.round(Math.random() * (100 - 30)) + this.typeSpeed;
            var self = this;
            // ------------- optional ------------- //
            // backpaces a certain string faster
            // ------------------------------------ //
            // if (self.arrayPos == 1){
            //  self.backDelay = 50;
            // }
            // else{ self.backDelay = 500; }
            // contain typing function in a timeout humanized delay
            self.timeout = setTimeout(function() {
                // check for an escape character before a pause value
                // format: \^\d+ .. eg: ^1000 .. should be able to print the ^ too using ^^
                // single ^ are removed from string
                var charPause = 0;
                var substr = curString.substr(curStrPos);
                if (substr.charAt(0) === "^") {
                    var skip = 1; // skip atleast 1
                    if (/^\^\d+/.test(substr)) {
                        substr = /\d+/.exec(substr)[0];
                        skip += substr.length;
                        charPause = parseInt(substr);
                    }
                    // strip out the escape character and pause value so they are not printed
                    curString = curString.substring(0, curStrPos) + curString.substring(curStrPos + skip);
                }
                if (self.contentType === "html") {
                    // skip over html tags while typing
                    var curChar = curString.substr(curStrPos).charAt(0)
                    if (curChar === "<" || curChar === "&") {
                        var tag = "";
                        var endTag = "";
                        if (curChar === "<") {
                            endTag = ">"
                        } else {
                            endTag = ";"
                        }
                        while (curString.substr(curStrPos).charAt(0) !== endTag) {
                            tag += curString.substr(curStrPos).charAt(0);
                            curStrPos++;
                        }
                        curStrPos++;
                        tag += endTag;
                    }
                }
                // timeout for any pause after a character
                self.timeout = setTimeout(function() {
                    if (curStrPos === curString.length) {
                        // fires callback function
                        self.options.onStringTyped(self.arrayPos);
                        // is this the final string
                        if (self.arrayPos === self.strings.length - 1) {
                            // animation that occurs on the last typed string
                            self.options.callback();
                            self.curLoop++;
                            // quit if we wont loop back
                            if (self.loop === false || self.curLoop === self.loopCount)
                                return;
                        }
                        self.timeout = setTimeout(function() {
                            self.backspace(curString, curStrPos);
                        }, self.backDelay);
                    } else {
                        /* call before functions if applicable */
                        if (curStrPos === 0)
                            self.options.preStringTyped(self.arrayPos);
                        // start typing each new char into existing string
                        // curString: arg, self.el.html: original text inside element
                        var nextString = curString.substr(0, curStrPos + 1);
                        if (self.attr) {
                            self.el.attr(self.attr, nextString);
                        } else {
                            if (self.isInput) {
                                self.el.val(nextString);
                            } else if (self.contentType === "html") {
                                self.el.html(nextString);
                            } else {
                                self.el.text(nextString);
                            }
                        }
                        // add characters one by one
                        curStrPos++;
                        // loop the function
                        self.typewrite(curString, curStrPos);
                    }
                    // end of character pause
                }, charPause);
                // humanized value for typing
            }, humanize);
        }
        ,
        backspace: function(curString, curStrPos) {
            // exit when stopped
            if (this.stop === true) {
                return;
            }
            // varying values for setTimeout during typing
            // can not be global since number changes each time loop is executed
            var humanize = Math.round(Math.random() * (100 - 30)) + this.backSpeed;
            var self = this;
            self.timeout = setTimeout(function() {
                // ----- this part is optional ----- //
                // check string array position
                // on the first string, only delete one word
                // the stopNum actually represents the amount of chars to
                // keep in the current string. In my case it is 14.
                // if (self.arrayPos == 1){
                //  self.stopNum = 14;
                // }
                //every other time, delete the whole typed string
                // else{
                //  self.stopNum = 0;
                // }
                if (self.contentType === "html") {
                    // skip over html tags while backspacing
                    if (curString.substr(curStrPos).charAt(0) === ">") {
                        var tag = "";
                        while (curString.substr(curStrPos).charAt(0) !== "<") {
                            tag -= curString.substr(curStrPos).charAt(0);
                            curStrPos--;
                        }
                        curStrPos--;
                        tag += "<";
                    }
                }
                // ----- continue important stuff ----- //
                // replace text with base text + typed characters
                var nextString = curString.substr(0, curStrPos);
                if (self.attr) {
                    self.el.attr(self.attr, nextString);
                } else {
                    if (self.isInput) {
                        self.el.val(nextString);
                    } else if (self.contentType === "html") {
                        self.el.html(nextString);
                    } else {
                        self.el.text(nextString);
                    }
                }
                // if the number (id of character in current string) is
                // less than the stop number, keep going
                if (curStrPos > self.stopNum) {
                    // subtract characters one by one
                    curStrPos--;
                    // loop the function
                    self.backspace(curString, curStrPos);
                }
                // if the stop number has been reached, increase
                // array position to next string
                else if (curStrPos <= self.stopNum) {
                    self.arrayPos++;
                    if (self.arrayPos === self.strings.length) {
                        self.arrayPos = 0;
                        // Shuffle sequence again
                        if(self.shuffle) self.sequence = self.shuffleArray(self.sequence);
                        self.init();
                    } else
                        self.typewrite(self.strings[self.sequence[self.arrayPos]], curStrPos);
                }
                // humanized value for typing
            }, humanize);
        }
        /**
         * Shuffles the numbers in the given array.
         * @param {Array} array
         * @returns {Array}
         */
        ,shuffleArray: function(array) {
            var tmp, current, top = array.length;
            if(top) while(--top) {
                current = Math.floor(Math.random() * (top + 1));
                tmp = array[current];
                array[current] = array[top];
                array[top] = tmp;
            }
            return array;
        }
        // Start & Stop currently not working
        // , stop: function() {
        //     var self = this;
        //     self.stop = true;
        //     clearInterval(self.timeout);
        // }
        // , start: function() {
        //     var self = this;
        //     if(self.stop === false)
        //        return;
        //     this.stop = false;
        //     this.init();
        // }
        // Reset and rebuild the element
        ,
        reset: function() {
            var self = this;
            clearInterval(self.timeout);
            var id = this.el.attr("id");
            this.el.after(\'<span id="\' + id + \'"/>\')
            this.el.remove();
            if (typeof this.cursor !== "undefined") {
                this.cursor.remove();
            }
            // Send the callback
            self.options.resetCallback();
        }
    };
    $.fn.typed = function(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data("typed"),
                options = typeof option == "object" && option;
            if (!data) $this.data("typed", (data = new Typed(this, options)));
            if (typeof option == "string") data[option]();
        });
    };
    $.fn.typed.defaults = {
        strings: ["These are the default values...", "You know what you should do?", "Use your own!", "Have a great day!"],
        stringsElement: null,
        // typing speed
        typeSpeed: 0,
        // time before typing starts
        startDelay: 0,
        // backspacing speed
        backSpeed: 0,
        // shuffle the strings
        shuffle: false,
        // time before backspacing
        backDelay: 500,
        // loop
        loop: false,
        // false = infinite
        loopCount: false,
        // show cursor
        showCursor: true,
        // character for cursor
        cursorChar: "|",
        // attribute to type (null == text)
        attr: null,
        // either html or text
        contentType: "html",
        // call when done callback function
        callback: function() {},
        // starting callback function before each string
        preStringTyped: function() {},
        //callback for every typed string
        onStringTyped: function() {},
        // callback for reset
        resetCallback: function() {}
    };
  }(window.jQuery);
  ';
  if($VIGOR === FALSE)$DIE='';
  die($DIE);
}

//  ######################################################################################################  //

//  LOAD CONFIG
if(error_reporting()==0){
  (@include_once("config-backup.php")) OR die("<tt><p><b>ERROR</b> Backup Config file not found!</p>");
}else{
  require_once("config-backup.php");
}

//  ######################################################################################################  //

//  HEADER
if (@$_SERVER["SESSIONNAME"] != "Console" and substr( php_sapi_name(), 0, 3 ) != "cli" ) {
  if ( file_exists('config-sql.php') ){
    $bak_db = ' &amp; db ';
  }else{
    $bak_db = '';
  }
  if ( file_exists('dropbox/dropbox.php') and version_compare(phpversion(), "5.3") > 0 ){
    $drop = ' 2 dropbox';
  }else{
    $drop = '';
  }
  echo '
	<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>ბექაფის გაკეთება</title>
		<link rel="icon" type="image/png" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAuklEQVQ4jd3SIW5CQRAG4C8vDQeo7AHQDQpRRSpIRRWKY6C4DRdogqxAovAEU9EDPFVFVjQg2Je32Sxki+yf/MlsZv5/Z2aXf4Uxtjhhh2mSe8EK39jgORePEKK44y9e8VbIhdxknRV0/Ios5dapQXul6Bbb1CBvsYYBmmhwyJdSgX16mLos7S+3T3LHOY4V4h+8X2trqP8LJX7iqWa+ZUG8qBGmmMWRQoyLeLhh8IFHDGJ8Fxr9UxdxBgBlYpFPJCb+AAAAAElFTkSuQmCC" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="'.basename(__FILE__).'?javascript=true" type="text/javascript"></script>
		<script>
			$(function(){
				$("#typed").typed({
					stringsElement: $("#typed-strings"),
					cursorChar: "_", // or _|∎
					typeSpeed: 0, // 30
				});
			});
		</script>
		<style>
			@import url(http://fonts.googleapis.com/css?family=Ubuntu:400,500);
			* {
				padding:0;
				margin:0;
			}
			body {
				font-family: "Ubuntu", sans-serif;
				font-size: 100%;
				background:#f8f8f8;
			}
			.wrap {
				max-width: 600px;
				margin:150px auto;
			}
			.text-editor-wrap {
				display: block;
				margin: auto;
				max-width: 530px;
				border-radius: 10px;
				box-shadow: rgba(0, 0, 0, 0.8) 0px 20px 70px;
				clear: both;
				overflow: hidden;
				-webkit-transition: all 0.5s ease-out;
				-moz-transition: all 0.5s ease-out;
				-ms-transition: all 0.5s ease-out;
				-o-transition: all 0.5s ease-out;
				transition: all 0.5s ease-out;
			}
			.title-bar {
				padding: 5px 0;
				font-family: "Lucida Grande", sans-serif;
				font-size: 0.75em;
				text-align: center;
				text-shadow: rgba(255, 255, 255, 0.8) 0px 1px 0px;
				background-color: #f8f8f8;
				background-image: -webkit-linear-gradient(top, #e8e8e8, #bcbbbc);
				background-image: -moz-linear-gradient(top, #e8e8e8, #bcbbbc);
				background-image: linear-gradient(top, #e8e8e8, #bcbbbc);
				box-shadow: inset rgba(255, 255, 255, 0.7) 0px 1px 1px;
				border-bottom: #6a6a6a 1px solid;
			}
			.text-body {
			';
			if($VIGOR === FALSE){
				echo '	height: 310px; /* BIG-WINDOW */';
			}else{
				echo '	height: 155px; /* SMALL-BOX */';
			}
			echo '
				background-color: rgba(0, 0, 0, 0.85);
				padding: 10px;
				color: #f0f0f0;
				text-shadow: #000 0px 1px 0px;
				font-family: "Consolas", "Courier New", "Courier";
				font-size: 1.75em;
				line-height: 1.40em;
				font-weight: 500;
				text-align: left;
				overflow: hidden;
				-webkit-transition: all 0.5s ease-out;
				-moz-transition: all 0.5s ease-out;
				-ms-transition: all 0.5s ease-out;
				-o-transition: all 0.5s ease-out;
				transition: all 0.5s ease-out;
			}
			.typed-cursor {
				opacity: 1;
				font-weight: 100;
				-webkit-animation: blink 0.7s infinite;
				-moz-animation: blink 0.7s infinite;
				-ms-animation: blink 0.7s infinite;
				-o-animation: blink 0.7s infinite;
				animation: blink 0.7s infinite;
			}
			@-keyframes blink {
				0% { opacity:1; }
				50% { opacity:0; }
				100% { opacity:1; }
			}
			@-webkit-keyframes blink {
				0% { opacity:1; }
				50% { opacity:0; }
				100% { opacity:1; }
			}
			@-moz-keyframes blink {
				0% { opacity:1; }
				50% { opacity:0; }
				100% { opacity:1; }
			}
			@-ms-keyframes blink {
				0% { opacity:1; }
				50% { opacity:0; }
				100% { opacity:1; }
			}
			@-o-keyframes blink {
				0% { opacity:1; }
				50% { opacity:0; }
				100% { opacity:1; }
			}
			a:hover {
				text-decoration: none;
			}
		</style>
	</head>
	<body>
		<div class="wrap">
			<div class="text-editor-wrap">
				<div class="title-bar">
					<span class="title">
						ბექაფის გაკეთება (Files'.$bak_db.' as '.$compression.$drop.')
					</span>
				</div>
				<div class="text-body">
					<div id="typed-strings">
	';
}

//  ######################################################################################################  //

//  CHECK PHP VERSION
if ( version_compare(phpversion(), "4.0") < 0 ) {
  die("<p><b>ERROR</b> PHP 4.0 or greater is required!</p>");
}

//  ######################################################################################################  //

//  START
if (@$_SERVER["SESSIONNAME"] != "Console" and substr( php_sapi_name(), 0, 3 ) != "cli" ) {
  echo "<p>Start&hellip;</p>";
}

//  ######################################################################################################  //

//  DELETE
if ( isset($_GET["del"]) and isset($_GET["del"]) ) {
  if(md5($_GET["file"].$salt) == $_GET["del"]){
    if(class_exists("FlxZipArchive")) {
      $ext = '.zip';
    }else{
      $ext = '.tar';
    }
    if(@unlink($backupDir.'/'.$_GET["file"].$ext)) {
      echo "<p>Delete File:<br>".$_GET["file"].$ext."<br><b>OK</b></p>";
    }else{
      echo "<p>File not found:<br>".$_GET["file"].$ext."<br><b>ERROR</b></p>";
    }
  }else{
    echo "<p>Wrong Parameter:<br><small>".str_replace('&','<br>',$_SERVER['QUERY_STRING'])."</small><br><b>ERROR</b></p>";
  }
  die('</div><span id="typed" style="*white-space:pre;"></span></div></div></body></html>');
}

//  ######################################################################################################  //

//  CHECK PASSWORD
session_start();

if ( @$_SERVER["SESSIONNAME"] != "Console" and substr( php_sapi_name(), 0, 3 ) != "cli" and $DEMO == FALSE ) {
	if ( isset($_GET['password']) ){ 
		$SALT = md5($password.$salt);
		if( (isset($_GET['password']) AND $_GET['password'] == $password) ) { 
		}else{
			die("<p><b>ERROR</b> Password wrong!</p>");
		}
	}else{
			die("<p><b>ERROR</b> No Password!</p>");
	}
}

//  ######################################################################################################  //

//  LOAD SQL CONFIG
if(error_reporting()==0){
  (@include_once("config-sql.php")) OR die("<p><b>ERROR</b> SQL Config file not found!</p>");
}else{
  require_once("config-sql.php");
}

//  ######################################################################################################  //

//  WORK
if(isset($DEMO) and $DEMO == FALSE){
  if ( strtolower( substr( substr( php_uname(), 0, strpos( php_uname(), " " ) ), 0, 3 ) ) !== "win" ) {
    $res;
    //  LINUX DAEMON
    if ( function_exists("pcntl_fork") ) {
      pcntl_fork(); //  FORK PROCESS
      if (@$_SERVER["SESSIONNAME"] === "Console" or substr( php_sapi_name(), 0, 3 ) == "cli" ) {
        print "\n\nDaemon running\n\n";
      } else {
        echo "<p>Daemon running&hellip;</p>";
      }
      posix_setsid(); //  MAKE CHILD PROCESS SESSION LEADER
      while (true) { //  DAEMON SCRIPT
        if(class_exists("FlxZipArchive")) {
          // ZIP
          $za = new FlxZipArchive;
          $res = $za->open($backupDir.'/'.$filename.".zip", ZipArchive::CREATE);
        }
        if($res === TRUE) {
          $za->addDir($backupDir,basename($backupDir));
          $za->close();
        }else{
          // TAR
          $archiv = new archiv( $filename.".tar" );
          chdir( $backupDir . "/" );
          backup( "./", $archiv );
          $archiv->write();
        }
      }
    } else {
      // LINUX
      if(class_exists("FlxZipArchive")) {
        // ZIP
        $za = new FlxZipArchive;
        $res = $za->open($backupDir.'/'.$filename.".zip", ZipArchive::CREATE);
      }
      if($res === TRUE) {
        $za->addDir($backupDir,basename($backupDir));
        $za->close();
      }else{
        // TAR
        $archiv = new archiv( $filename.".tar" );
        chdir( $backupDir . "/" );
        backup( "./", $archiv );
        $archiv->write();
      }
    }
  } else {
    //  WINDOWS
    if(class_exists("FlxZipArchive")) {
      // ZIP
      $za = new FlxZipArchive;
      $res = $za->open($backupDir.'/'.$filename.".zip", ZipArchive::CREATE);
    }
    if($res === TRUE) {
      $za->addDir($backupDir,basename($backupDir));
      $za->close();
    }else{
      // TAR
      $archiv = new archiv( $filename.".tar" );
      chdir( $backupDir . "/" );
      backup( "./", $archiv );
      $archiv->write();
    }
  }
  if($res === TRUE) {
    if ( @$_SERVER["SESSIONNAME"] === "Console" or substr(php_sapi_name(), 0, 3) == "cli") {
    }else{
      echo "<p>Write .ZIP File&hellip;</p>";
    }
  }
}else{
  if ( @$_SERVER["SESSIONNAME"] === "Console" or substr( php_sapi_name(), 0, 3 ) == "cli" ) {
  } elseif($count < 1) {
    $count++;
    echo "<p>Read Folder for .ZIP File&hellip;</p>";
    echo "<p>Write .ZIP File&hellip;</p>";
  }
}
//  DEL .sql - THEY ARE IN THE .TAR
if ( isset($sql) ) {
  foreach ($sql as $file){
    @unlink($backupDir.'/'.$file);
  }
}
//  IF ADDRESS IS SET THEN SEND EMAIL
if ( isset($to) and is_valid_email_address($to) ) {
  send_mail( $to, $toCc, $toBcc );
}

//  ######################################################################################################  //

//  DROPBOX - PLUGIN
if(class_exists("FlxZipArchive")) {
  $ext = '.zip';
}else{
  $ext = '.tar';
}
if ( version_compare(phpversion(), "5.3") > 0 ) {
  if ( file_exists('config-dropbox.php') ) {
    include('config-dropbox.php');
  }
  if ( file_exists('dropbox/dropbox.php') and 
       isset($boxKey) and $boxKey != '' and
       isset($boxSecret) and $boxSecret != ''
     ) {
    include_once('dropbox/dropbox.php');
    $files[] = $backupDir.'/'.$filename.$ext;
    echo DropBox_Upload($files);
  }
}

//  ######################################################################################################  //

//  FOOTER
if ( @$_SERVER["SESSIONNAME"] === "Console" or substr( php_sapi_name(), 0, 3 ) == "cli" ) {
  print "\r     ";
} else {
  echo "<p>Ready, please download ";
  $download_url = $backupUrl."/".$filename.$ext;
  #if(isset($DEMO) and $DEMO == TRUE)$download_url = '#';
  // echo "<a style=\"color:white;\" href=\"".$download_url."\">";
  // echo strtoupper(ltrim($ext,'.'))."</a> ";
  // echo "<b style=\"color:red;\">&#10084;</b>"; // HEART
  echo '
				</div>
				<span id="typed" style="*white-space:pre;">
				</span>
			</div>
		</div>
	</body>
</html>
';
}
//  ######################################################################################################  //

//  WORKER
function backup( $dir, &$archiv ) {
  global $filename;
  global $count;
  $files = array();
  $d = dir( $dir );
  while ( false !== ( $entry = $d->read() ) )
    $files[] = $entry;
  $d->close();
  foreach ( $files as $file ) {
    if ( $file[0] !== "." && $file!== $filename.".tar" ) {
      if ( is_dir( $dir . $file ) ) {
        backup( $dir . $file . "/", $archiv );
      } elseif ( is_file( $dir . $file ) ) {
        $_dir = str_replace( "./", "",  $dir );
        if ( @$_SERVER["SESSIONNAME"] === "Console" or substr( php_sapi_name(), 0, 3 ) == "cli" ) {
          $z++;
          if ( $z === 10 ) print "[/]\r";
          if ( $z === 20) print "[-]\r";
          if ( $z === 30 ) {
            print "[\]\r";
            $z = 0;
          }
        }
        $archiv->add( $_dir . $file );
      }
    }
  }
  if ( @$_SERVER["SESSIONNAME"] === "Console" or substr( php_sapi_name(), 0, 3 ) == "cli" ) {
  } elseif($count < 1) {
    $count++;
    echo "<p>Read Folder for .TAR File&hellip;</p>";
  }
}

//  ######################################################################################################  //

function send_mail( $to, $toCc="", $toBcc="" ) {
  global $filename;
  global $backupUrl;
  global $scriptUrl;
  global $DEMO;
  $subject  = "[BACKUP] ".$filename;
  $headers  = "From: " . $to . "\r\n";
  $headers .= "Reply-To: ". $to . "\r\n";
  if ( isset($toCc) and is_valid_email_address($toCc) ) {
    $headers .= "CC: ". $toCc ."\r\n";
  }
  if ( isset($toBcc) and is_valid_email_address($toBcc) ) {
    $headers .= "BCC: ". $toBcc ."\r\n";
  }
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=utf-8\r\n";
  $headers .= "X-Mailer: PHP ". phpversion();
  $message  = "<html>\n";
  $message  = "<body>\n";
  $message  = "<h2><tt>Simple File Backup</tt></h2>\n";
  $message .= "<table rules=\"all\" style=\"border-color: #666;\" cellpadding=\"10\">\n";
  $message .= "<tr>\n";
  $message .= "<td colspan=\"2\"></td>\n";
  $message .= "</tr>\n";
  $message .= "<tr style=\"background: #eee;\">\n";
  $message .= "<td><strong>Action</strong></td>\n";
  $message .= "<td><strong>Link</strong></td>\n";
  $message .= "</tr>\n";
  $message .= "<tr>\n";
  $message .= "<td>Download Backup</td>\n";
  $css      = " style=\"background-color:#128DEB;color:white;text-decoration:none;padding:0 7px;\" ";
  if(class_exists("FlxZipArchive")) {
    $ext = '.zip';
  }else{
    $ext = '.tar';
  }
  $message .= "<td><a".$css."href=\"".strip_tags($backupUrl."/".$filename.$ext)."\">";
  $message .=                         strip_tags($backupUrl."/".$filename.$ext)."</a></td>\n";
  $message .= "</tr>\n";
  $message .= "<tr>\n";
  $message .= "<td>Delete Backup</td>\n";
  $host = 'http'.(!empty($_SERVER['HTTPS'])?'s':'').':/'.'/'.$_SERVER['SERVER_NAME'].strtok($_SERVER["REQUEST_URI"],'?');

  $url      = $scriptUrl."?del=".md5($filename.$salt)."&file=".$filename;
  $css      = " style=\"background-color:#FF0039;color:white;text-decoration:none;padding:0 7px;\" ";
  $message .= "<td><a".$css."href=\"".strip_tags($url)."\">".strip_tags($url)."</a></td>\n";
  $message .= "</tr>\n";
  $message .= "<tr>\n";
  $message .= "<td colspan=\"2\"></td>\n";
  $message .= "</tr>\n";
  $message .= "</table>\n";
  $message .= "</body>\n";
  $message .= "</html>\n";
  if ( isset($DEMO) and $DEMO == TRUE ) {
    if ( @$_SERVER["SESSIONNAME"] === "Console" or substr( php_sapi_name(), 0, 3 ) == "cli" ) {
      print "\nDOWNLOAD ".strip_tags($backupUrl."/".$filename.$ext)."\n";
      print "DELETE   ".strip_tags($url)."\n";
    }else{
      echo "<p>Would eMail to Admin&hellip;</p>";
    }
  }elseif(@mail($to, $subject, $message, $headers)){
    if ( @$_SERVER["SESSIONNAME"] === "Console" or substr(php_sapi_name(), 0, 3) == "cli") {
    }else{
      echo "<p>Send eMail to Admin&hellip;</p>";
    }
  }
}

//  ######################################################################################################  //

function db_backup($dbhost, $dbuser, $dbpwd, $dbname) {
  global $filename;
  global $backupDir;
  $conn = @mysql_connect($dbhost, $dbuser, $dbpwd) or die("<p><b>ERROR</b> mySQL Access denied&hellip;</p>");
  mysql_select_db($dbname);
  $tables = mysql_list_tables($dbname) or die("<p><b>ERROR</b> Table '".$dbname."' not exists&hellip;</p>");
  $f = fopen($backupDir.'/'.$filename."_".$dbname.".sql", "w");
  if (@$_SERVER["SESSIONNAME"] === "Console" or substr( php_sapi_name(), 0, 3 ) == "cli" ) {
  } else {
    echo "<p>Copy db to .sql File&hellip;</p>";
  }
  while ($cells = mysql_fetch_array($tables)) {
    $table = $cells[0];
    fwrite($f,"DROP TABLE IF EXISTS `".$table."`;\n"); 
    $res = mysql_query("SHOW CREATE TABLE `".$table."`");
    if ($res) {
      $create = mysql_fetch_array($res);
      $create[1] .= ";";
      $line = str_replace("\n", "", $create[1]);
      fwrite($f, $line."\n");
      $data = mysql_query("SELECT * FROM `".$table."`");
      $num = mysql_num_fields($data);
      while ($row = mysql_fetch_array($data)) {
        $line = "INSERT INTO `".$table."` VALUES(";
        for ($i=1;$i<=$num;$i++) {
            $line .= "'".mysql_escape_mimic($row[$i-1])."', "; // mysql_real_escape_string
        }
        $line = substr($line,0,-2);
        fwrite($f, $line.");\n");
      }
    }
  }
  fclose($f);
  if(isset($DEMO) and $DEMO == TRUE)@unlink($backupDir.'/'.$filename."_".$dbname.".sql");
  return $filename."_".$dbname.".sql";
}

//  ######################################################################################################  //

function mysql_escape_mimic($inp) {
  if(is_array($inp))
    return array_map(__METHOD__, $inp);
  if(!empty($inp) && is_string($inp)) {
    return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
  }
  return $inp;
}

//  ######################################################################################################  //

function is_valid_email_address($email){
  $qtext          = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
  $dtext          = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
  $atom           = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
  $quoted_pair    = '\\x5c[\\x00-\\x7f]';
  $domain_literal = "\\x5b($dtext|$quoted_pair)*\\x5d";
  $quoted_string  = "\\x22($qtext|$quoted_pair)*\\x22";
  $domain_ref     = $atom;
  $sub_domain     = "($domain_ref|$domain_literal)";
  $word           = "($atom|$quoted_string)";
  $domain         = "$sub_domain(\\x2e$sub_domain)*";
  $local_part     = "$word(\\x2e$word)*";
  $addr_spec      = "$local_part\\x40$domain";
  return preg_match("!^$addr_spec$!", $email) ? 1 : 0;
}

//  ######################################################################################################  //

class archiv {
  /**
   * The name of the tar-file to create.
   *
   * @var string
   */
  private $filename;
  /**
   * The list of files to add to the archive.
   *
   * @var array
   */
  private $filelist=array();
  /**
   * Constructor
   *
   * @param string $filename
   */
  public function __construct($filename)
  {
    $this->filename=$filename;
  }
  /**
   * Add a file.
   *
   * @param string $filename
   */
  public function add($filename)
  {
    if ((file_exists($filename)) && (is_readable($filename)))
    {
      $this->filelist[]=$filename;
    }
  }
  /**
   * Write the tar-file.
   *
   * @return bool
   */
  public function write()
  {
    sort($this->filelist);
    $tarfile=@fopen($this->filename,"w");
    if ($tarfile==false)
    {
      return false;
    }
    for ($x=0;$x<count($this->filelist);$x++)
    {
      $filename=$this->filelist[$x];
      if ((is_dir($this->filelist[$x])) && (substr($this->filelist[$x],-1)!="/"))
      {
        $filename.="/";
      }
      while (strlen($filename)<100)
      {
        $filename.=chr(0);
      }
      $permissions=sprintf("%o",fileperms($this->filelist[$x])).chr(0);
      while (strlen($permissions)<8)
      {
        $permissions="0".$permissions;
      }
      $userid=sprintf("%o",fileowner($this->filelist[$x])).chr(0);
      while (strlen($userid)<8)
      {
        $userid="0".$userid;
      }
      $groupid=sprintf("%o",filegroup($this->filelist[$x])).chr(0);
      while (strlen($groupid)<8)
      {
        $groupid="0".$groupid;
      }
      if (is_dir($this->filelist[$x]))
      {
        $filesize="0".chr(0);
      }
      else
      {
        $filesize=sprintf("%o",filesize($this->filelist[$x])).chr(0);
      }
      while (strlen($filesize)<12)
      {
        $filesize="0".$filesize;
      }
      $modtime=sprintf("%o",filectime($this->filelist[$x])).chr(0);
      $checksum="        ";
      if (is_dir($this->filelist[$x]))
      {
        $indicator=5;
      }
      else
      {
        $indicator=0;
      }
      $linkname="";
      while (strlen($linkname)<100)
      {
        $linkname.=chr(0);
      }
      $ustar="ustar  ".chr(0);
      if (function_exists("posix_getpwuid"))
      {
        $user=posix_getpwuid(octdec($userid));
        $user=$user["name"];
      }
      else
      {
        $user="";
      }
      while (strlen($user)<32)
      {
        $user.=chr(0);
      }
      if (function_exists("posix_getgrgid"))
      {
        $group=posix_getgrgid(octdec($groupid));
        $group=$group["name"];
      }
      else
      {
        $group="";
      }
      while (strlen($group)<32)
      {
        $group.=chr(0);
      }
      $devmajor="";
      while (strlen($devmajor)<8)
      {
        $devmajor.=chr(0);
      }
      $devminor="";
      while (strlen($devminor)<8)
      {
        $devminor.=chr(0);
      }
      $prefix="";
      while (strlen($prefix)<155)
      {
        $prefix.=chr(0);
      }
      $header=$filename.$permissions.$userid.$groupid.$filesize.$modtime.$checksum.$indicator;
      $header.=$linkname.$ustar.$user.$group.$devmajor.$devminor.$prefix;
      while (strlen($header)<512)
      {
        $header.=chr(0);
      }
      $checksum=0;
      for ($y=0;$y<strlen($header);$y++)
      {
        $checksum+=ord($header[$y]);
      }
      $checksum=sprintf("%o",$checksum).chr(0)." ";
      while (strlen($checksum)<8)
      {
        $checksum="0".$checksum;
      }
      $header=$filename.$permissions.$userid.$groupid.$filesize.$modtime.$checksum.$indicator;
      $header.=$linkname.$ustar.$user.$group.$devmajor.$devminor.$prefix;
      while (strlen($header)<512)
      {
        $header.=chr(0);
      }
      fwrite($tarfile,$header);
      if ($indicator==0)
      {
        if ( @$_SERVER["SESSIONNAME"] === "Console" or substr(php_sapi_name(), 0, 3) == "cli") {
          $z++;
          if ( $z === 10 ) print "[/]\r";
          if ( $z === 20 ) print "[-]\r";
          if ( $z === 30 ) { 
            print "[\]\r";
            $z = 0;
          }
        }
        $contentfile=fopen($this->filelist[$x],"r");
        // @ = "Error Control Operator" on "fread" Operator
        // otherwise with 0 byte files a warning is issued
        $data=@fread($contentfile,filesize($this->filelist[$x]));
        while (strlen($data)%512!=0)
        {
          $data.=chr(0);
        }
        fwrite($tarfile,$data);
      }
    }
    fclose($tarfile);
    if ( @$_SERVER["SESSIONNAME"] === "Console" or substr(php_sapi_name(), 0, 3) == "cli") {
    }else{
      echo "<p>Write .TAR File&hellip;</p>";
    }
    return true;
  }
}

//  ######################################################################################################  //
//  EOF - End of File                                                                                       //
//  ######################################################################################################  //


// Move file when backup is finish
rename("../../".$filename.$ext, "../../../".$filename.$ext);